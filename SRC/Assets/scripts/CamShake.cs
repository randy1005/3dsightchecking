﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamShake : MonoBehaviour {

    public IEnumerator Shake(float duration, float magnitude){
        Vector3 origPos = transform.localPosition;

        float elapsed = 0.0f;
        while(elapsed < duration){ // keep shaking

            // offsets on the x,y axis to move the camera
            float x_shake = Random.Range(-1f, 1f) * magnitude;
            float y_shake = Random.Range(-1f, 1f) * magnitude;

            // apply offsets
            transform.position = new Vector3(x_shake, y_shake, origPos.z);

            elapsed += Time.deltaTime; //update time, or else shakes forever
            yield return null; // this means we want to wait until the frame is updated, then do this loop again
        }

        // reset position
        transform.localPosition = origPos;
    }
}
