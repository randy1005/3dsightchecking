﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class PantieControl : MonoBehaviour {

    /*for spawning panties*/
    public GameObject pantiePrefab;
    public Vector3 left;
    public Vector3 right;
    public AudioClip[] audioClips;
    public new AudioSource audio;



    public static int pantieCount = 3;
    public static int hitUnityChan = 0;

    // camera shaking
    public CamShake camShake;

    // penalty
    public Penalty penalty;

    /*for hitting panties with mouse*/
    private Ray ray;
    private RaycastHit rayCastHit;
    private Vector3 targetPos;

    /*random spawn time*/
    float spawnTime;
    float time;

	// Use this for initialization
	void Start () {

        /*set spawn time for panties*/
        SetRandomTime();
        time = 0;
        audio = GetComponent<AudioSource>();
	}

    // sfx
    void playSFX(int clip){
        audio.clip = audioClips[clip];
        audio.Play();
    }


    // Random spawn panties
    void spawnPanties()
    {
        Transform pantie = Instantiate(pantiePrefab.transform);
        int flag = Random.Range(-1, 1);
        Vector3 pos = (flag == -1) ? left : right;
        pantie.transform.position = pos;
        Rigidbody rb = pantie.GetComponent<Rigidbody>();
        rb.velocity = (flag == -1) ? new Vector3(1f, 4.5f, 0) : new Vector3(-1f, 4.5f, 0);
        // whoosh sound
        playSFX(0);
    }

    // Sets the random time between minTime and maxTime
    void SetRandomTime()
    {
        spawnTime = Random.Range(10, 20);
        Debug.Log("Next object spawn in " + spawnTime + " seconds.");
    }

    void FixedUpdate()
    {
        //Counts up
        time += Time.deltaTime;

        //Check if its the right time to spawn the object
        if (time >= spawnTime)
        {
            Debug.Log("Time to spawn: " + pantiePrefab.name);
            spawnPanties();
            SetRandomTime();
            time = 0;
        }

        // raycasthit
        if(Input.GetMouseButtonDown(0))
            RayHit();
    }

    void RayHit()
    {
        // ray coming out of mouse position
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out rayCastHit, 4000))
        { 
            //use 2000 to specify how far the ray would go
            //Debug.Log(rayCastHit.transform.name);
            //pointerPos = rayCastHit.point;
 

            float hitForce = 1200f;


            if (rayCastHit.transform.name == "Pantie(Clone)")
            {
                Debug.Log("ray hit panties");
                // bump pantie out of way
                rayCastHit.rigidbody.AddForce(hitForce * new Vector3(0, 0, 1f));
                pantieCount++;

                // hit marker sound
                playSFX(1);

                Destroy(rayCastHit.transform.gameObject, 2f);
            }
            else if(rayCastHit.transform.name == "unitychan_up(Clone)" || 
                    rayCastHit.transform.name == "unitychan_down(Clone)" ||
                    rayCastHit.transform.name == "unitychan_left(Clone)" ||
                    rayCastHit.transform.name == "unitychan_right(Clone)")
            {
                Debug.Log("ray hit unitychan");
                int clipMoan = Random.Range(2, 5);
                playSFX(clipMoan);

                //StartCoroutine(camShake.Shake(.2f, .4f));
                CameraShaker.Instance.ShakeOnce(4f, 4f, .1f, 1f);
                pantieCount -= 1;
                hitUnityChan++;

                // pentalty for hitting unity chan too many times
                if(hitUnityChan >= 5){
                    Debug.Log("Penalty");
                    playSFX(5);
                    CameraShaker.Instance.ShakeOnce(15f, 6f, 5f, 5f);
                    pantieCount -= 5;
                    StartCoroutine(penalty.PantieTsunami(150));
                    hitUnityChan = 0;
                }
            }
        }
    }

}
