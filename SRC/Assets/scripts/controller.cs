﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controller : MonoBehaviour {
    public AudioSource audio;
    public AudioClip[] audioClip2;
    int combo = 0;

	// Use this for initialization
	void Start () {
        main = GetComponent<Camera>();
        t = 0;
        avatars = new List<Transform>();
        generate();
        focus_target = 0;
        audio = main.GetComponent<AudioSource>();
    }

	
	// Update is called once per frame
	void Update () {
        main.fieldOfView = 60;
        if (Input.GetKey(KeyCode.Space)) main.fieldOfView = 30;
        t += Time.deltaTime;
        if (t > 2)
        {
            t = 0;
            generate();
        }

        if (avatars.Count != 0)
        {
            if (avatars[0].position.z < -4)
            {
                Destroy(avatars[0].gameObject);
                avatars.RemoveAt(0);
            }
            for (int i = 0; i < avatars.Count; i++)
            {
                avatars[i].position -= new Vector3(0, 0, 25 * Time.deltaTime);
            }

            // sfx
            //if (combo == 0)
            //{
            //    audio.clip = audioClip2[0];
            //}
            //else if (combo == 1)
            //{
            //    audio.clip = audioClip2[1];
            //}
            //else if (combo == 2)
            //{
            //    audio.clip = audioClip2[2];
            //}

            if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                if (avatars[0].name == "unitychan_up(Clone)")
                {
                    //combo++;
                    audio.Play();

                    Destroy(avatars[0].GetChild(0).gameObject);
                    avatars[0].GetComponent<Animator>().SetBool("dead", true);
                    Destroy(avatars[0].gameObject, 1.5f);
                    avatars.RemoveAt(0);
                }
                else
                {
                    //audio.clip = audioClip2[3];
                    //audio.Play();
                    //combo = 0;
                }

            }
            else if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                if (avatars[0].name == "unitychan_down(Clone)")
                {
                    //combo++;
                    //audio.Play();

                    Destroy(avatars[0].GetChild(0).gameObject);
                    avatars[0].GetComponent<Animator>().SetBool("dead", true);
                    Destroy(avatars[0].gameObject, 1.5f);
                    avatars.RemoveAt(0);
                }
                else
                {
                    //audio.clip = audioClip2[3];
                    //audio.Play();
                    //combo = 0;
                }
            }
            else if (Input.GetKeyUp(KeyCode.LeftArrow))
            {
                if (avatars[0].name == "unitychan_left(Clone)")
                {
                    //combo++;
                    //audio.Play();

                    Destroy(avatars[0].GetChild(0).gameObject);
                    avatars[0].GetComponent<Animator>().SetBool("dead", true);
                    Destroy(avatars[0].gameObject, 1.5f);
                    avatars.RemoveAt(0);
                }
                else
                {
                    //audio.clip = audioClip2[3];
                    //audio.Play();
                    //combo = 0;
                }
            }
            else if (Input.GetKeyUp(KeyCode.RightArrow))
            {
                if (avatars[0].name == "unitychan_right(Clone)")
                {
                    //combo++;
                    //audio.Play();

                    Destroy(avatars[0].GetChild(0).gameObject);
                    avatars[0].GetComponent<Animator>().SetBool("dead", true);
                    Destroy(avatars[0].gameObject, 1.5f);
                    avatars.RemoveAt(0);
                }
                else
                {
                    //audio.clip = audioClip2[3];
                    //audio.Play();
                    //combo = 0;
                }
            }
        }
    }

    void generate()
    {
        int dir = Random.Range(0,3);
        float pos_x = Random.Range(-2, 2);
        float pos_y = 0;
        Transform new_avatar;
        switch (dir){
            case 0:
                new_avatar = Instantiate(up);
                new_avatar.position = new Vector3(pos_x, pos_y, 150);
                avatars.Add(new_avatar);
                break;
            case 1:
                new_avatar = Instantiate(down);
                new_avatar.position = new Vector3(pos_x, pos_y, 150);
                avatars.Add(new_avatar);
                break;
            case 2:
                new_avatar = Instantiate(left);
                new_avatar.position = new Vector3(pos_x, pos_y, 150);
                avatars.Add(new_avatar);
                break;
            case 3:
                new_avatar = Instantiate(right);
                new_avatar.position = new Vector3(pos_x, pos_y, 150);
                avatars.Add(new_avatar);
                break;
        }
        
    }
    public Transform up;
    public Transform down;
    public Transform left;
    public Transform right;
    public List <Transform> avatars;
    public int focus_target;
    double t;
    Camera main;
}
