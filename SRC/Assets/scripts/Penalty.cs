﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Penalty : MonoBehaviour {

    Vector3 spawnPt;
    public GameObject pantiePrefab;
    public IEnumerator PantieTsunami(int pantieAmount){
        
        for (int i = 0; i < pantieAmount; i++){
            float spawnX = Random.Range(-2, 4);
            float spawnY = Random.Range(0, 2);
            float spawnZ = 40f;
            spawnPt = new Vector3(spawnX, spawnY, spawnZ);
            Transform t = Instantiate(pantiePrefab.transform);
            t.position = spawnPt;
            Rigidbody rb = t.GetComponent<Rigidbody>();
            rb.useGravity = false;
            rb.AddForce(0, 0, -1000f);
            yield return 0;
        }
        
    }
}
